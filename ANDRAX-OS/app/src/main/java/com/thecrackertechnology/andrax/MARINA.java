package com.thecrackertechnology.andrax;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class MARINA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /**
         *
         * Help me, i'm dying...
         *
         **/

        run_hack_cmd("sudo marina");

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

}
